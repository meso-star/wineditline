cmake_minimum_required(VERSION 2.6)
include(FindPackageHandleStandardArgs)

find_path(WinEditLine_INCLUDE_DIR wineditline/readline.h)
find_library(WinEditLine_LIBRARY wineditline PATH_SUFFIXES bin)
find_library(WinEditLine_LIBRARY_DEBUG wineditline-dbg PATH_SUFFIXES bin)

if(WinEditLine_LIBRARY OR WinEditLine_LIBRARY_DEBUG)
  if(NOT WinEditLine_LIBRARY_DEBUG AND WinEditLine_LIBRARY)
    set(WinEditLine_LIBRARY_DEBUG ${WinEditLine_LIBRARY})
  elseif(NOT WinEditLine_LIBRARY AND WinEditLine_LIBRARY_DEBUG)
    set(WinEditLine_LIBRARY ${WinEditLine_LIBRARY_DEBUG})
  endif(NOT WinEditLine_LIBRARY_DEBUG AND WinEditLine_LIBRARY)

  add_library(WinEditLine SHARED IMPORTED)
  set(WinEditLine_STATIC_LIBRARY OFF CACHE INTERNAL "")
  set_target_properties(WinEditLine PROPERTIES
    IMPORTED_IMPLIB ${WinEditLine_LIBRARY}
    IMPORTED_IMPLIB_DEBUG ${WinEditLine_LIBRARY_DEBUG}
    IMPORTED_IMPLIB_RELEASE ${WinEditLine_LIBRARY})

else(WinEditLine_LIBRARY OR WinEditLine_LIBRARY_DEBUG)

  find_library(WinEditLine_LIBRARY wineditline_static PATH_SUFFIXES bin)
  find_library(WinEditLine_LIBRARY_DEBUG wineditline_static-dbg PATH_SUFFIXES bin)
  if(NOT WinEditLine_LIBRARY_DEBUG AND WinEditLine_LIBRARY)
    set(WinEditLine_LIBRARY_DEBUG ${WinEditLine_LIBRARY})
  elseif(NOT WinEditLine_LIBRARY AND WinEditLine_LIBRARY_DEBUG)
    set(WinEditLine_LIBRARY ${WinEditLine_LIBRARY_DEBUG})
  endif(NOT WinEditLine_LIBRARY_DEBUG AND WinEditLine_LIBRARY)

  add_library(WinEditLine STATIC IMPORTED)
  set(WinEditLine_STATIC_LIBRARY ON CACHE INTERNAL "")
  set_target_properties(WinEditLine PROPERTIES
      IMPORTED_LOCATION ${WinEditLine_LIBRARY}
      IMPORTED_LOCATION_DEBUG ${WinEditLine_LIBRARY_DEBUG}
      IMPORTED_LOCATION_RELEASE ${WinEditLine_LIBRARY})

endif(WinEditLine_LIBRARY OR WinEditLine_LIBRARY_DEBUG)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(WinEditLine DEFAULT_MSG
  WinEditLine_INCLUDE_DIR
  WinEditLine_LIBRARY
  WinEditLine_LIBRARY_DEBUG)

